_base_ = [
    "res50_coco.py",
]

data_root = "data/bop"
dataset = 'ycbv'
scene = '000050'
work_dir=f'work_dirs/res50_coco/bop/{dataset}/{scene}'

data = dict(
    test=dict(
        type="TopDownMetagraspnetDataset",
        ann_file=f"{data_root}/test_coco_annotations/{dataset}/test/{scene}/filtered_scene_gt_coco.json",
        img_prefix=f"{data_root}/{dataset}/test/{scene}/",
    ),
)


