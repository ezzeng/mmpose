
_base_ = [
    "../_base_/default_runtime.py",
    "../_base_/datasets/metagraspnet/class_info_2_cats.py"
]

evaluation = dict(interval=5, metric="mAP", save_best="AP")

optimizer = dict(
    type="Adam",
    lr=5e-4,
)
optimizer_config = dict(grad_clip=None)
# learning policy
lr_config = dict(
    policy="step",
    warmup="linear",
    warmup_iters=500,
    warmup_ratio=0.001,
    step=[170, 200],
)
total_epochs = 210

heatmap_size = [64, 64]
# model settings
model = dict(
    type="MultiheadTopDown",
    pretrained="torchvision://resnet50",
    backbone=dict(type="ResNet", depth=50),
    category_ids={{_base_.CATEGORY_IDS}},
    num_keypoints={{_base_.NUM_KEYPOINTS}},
    heads_config=dict(
        type="TopdownHeatmapSimpleHead",
        in_channels=2048,
        loss_keypoint=dict(type="JointsMSELoss", use_target_weight=True),
    ),
    train_cfg=dict(),
    test_cfg=dict(
        flip_test=True, post_process="default", shift_heatmap=True, modulate_kernel=11
    ),
    heatmap_size=heatmap_size,
)

data_cfg = dict(
    image_size=[256, 256],
    heatmap_size=heatmap_size,
    num_output_channels={{_base_.NUM_KEYPOINTS}},
    num_joints={{_base_.NUM_KEYPOINTS}},
    dataset_channel={{_base_.dataset_channel}},
    inference_channel={{_base_.dataset_channel}},
    soft_nms=False,
    nms_thr=1.0,
    oks_thr=0.9,
    vis_thr=0.2,
    use_gt_bbox=True,
    det_bbox_thr=0.0,
    bbox_file="",
)

train_pipeline = [
    dict(type="LoadImageFromFile"),
    # dict(type="TopDownRandomFlip", flip_prob=0.5),
    # dict(type="TopDownHalfBodyTransform", num_joints_half_body=8, prob_half_body=0.3),
    # dict(type="TopDownGetRandomScaleRotation", rot_factor=40, scale_factor=0.5),
    dict(type="TopDownAffine"),
    dict(type="ToTensor"),
    dict(type="NormalizeTensor", mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
    dict(type="TopDownGenerateTarget", sigma=2),
    dict(
        type="Collect",
        keys=["img", "target", "target_weight"],
        meta_keys=[
            "image_file",
            "joints_3d",
            "joints_3d_visible",
            "center",
            "scale",
            "rotation",
            "bbox_score",
            "flip_pairs",
            "category_id",
        ],
    ),
]

val_pipeline = [
    dict(type="LoadImageFromFile"),
    dict(type="TopDownAffine"),
    dict(type="ToTensor"),
    dict(type="NormalizeTensor", mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
    dict(
        type="Collect",
        keys=["img"],
        meta_keys=[
            "image_file",
            "center",
            "scale",
            "rotation",
            "bbox_score",
            "flip_pairs",
            "category_id",
        ],
    ),
]

test_pipeline = val_pipeline

data_root = "data"
data = dict(
    samples_per_gpu=64,
    workers_per_gpu=2,
    val_dataloader=dict(samples_per_gpu=32),
    test_dataloader=dict(samples_per_gpu=32),
    single_category_per_batch=True,
    train=dict(
        type="TopDownMetagraspnetDataset",
        ann_file=f"{data_root}/annotations/0517/2_categories/train_edited.json",
        img_prefix=f"{data_root}/images/",
        data_cfg=data_cfg,
        pipeline=train_pipeline,
        classes_info={{_base_.classes_info}},
    ),
    val=dict(
        type="TopDownMetagraspnetDataset",
        ann_file=f"{data_root}/annotations/0517/2_categories/val_edited.json",
        img_prefix=f"{data_root}/images/",
        data_cfg=data_cfg,
        pipeline=val_pipeline,
        classes_info={{_base_.classes_info}},
    ),
    test=dict(
        type="TopDownMetagraspnetDataset",
        ann_file=f"{data_root}/annotations/0517/2_categories/test_edited.json",
        img_prefix=f"{data_root}/images/",
        data_cfg=data_cfg,
        pipeline=val_pipeline,
        classes_info={{_base_.classes_info}},
    ),
)
