CATEGORY_IDS = {
    "cracker_box": 0,
    "sugar_box": 1,
    "tomato_soup_can": 2,
    "mustard_bottle": 3,
    "potted_meat_can": 4,
    "banana": 5,
    "bowl": 6,
    "mug": 7,
    "power_drill": 8,
    "scissor": 9,
    "chips_can": 10,
    "strawberry": 11,
    "apple": 12,
    "lemon": 13,
    "peach": 14,
    "pear": 15,
    "orange": 16,
    "plum": 17,
    "knife": 18,
    "phillips_screwdriver": 19,
    "flat_screwdriver": 20,
    "racquetball": 21,
    "b_cups": 22,
    "d_cups": 23,
    "a_toy_airplane": 24,
    "c_toy_airplane": 25,
    "d_toy_airplane": 26,
    "f_toy_airplane": 27,
    "h_toy_airplane": 28,
    "i_toy_airplane": 29,
    "j_toy_airplane": 30,
    "k_toy_airplane": 31,
    "light_bulb": 32,
    "kitchen_knife": 34,
    "screw_valve": 35,
    "plastic_pipes": 36,
    "cables_in_transparent_bag": 37,
    "cables": 38,
    "wire_cutter": 39,
    "desinfection": 40,
    "hairspray": 41,
    "handcream": 42,
    "toothpaste": 43,
    "toydog": 44,
    "sponge": 45,
    "pneumatic_cylinder": 46,
    "airfilter": 47,
    "coffeefilter": 48,
    "wash_glove": 49,
    "wash_sponge": 50,
    "garbage_bags": 51,
    "deo": 52,
    "cat_milk": 53,
    "bottle_glass": 54,
    "bottle_press_head": 55,
    "shaving_cream": 56,
    "chewing_gum_with_spray": 57,
    "lighters": 58,
    "cream_soap": 59,
    "box_1": 60,
    "box_2": 61,
    "box_3": 62,
    "box_4": 63,
    "box_5": 64,
    "box_6": 65,
    "box_7": 66,
    "box_8": 67,
    "glass_cup": 68,
    "tennis_ball": 69,
    "cup": 70,
    "wineglass": 71,
    "handsaw": 72,
    "lipcare": 73,
    "woodcube_a": 74,
    "lipstick": 75,
    "nosespray": 76,
    "tape": 77,
    "bookholder": 78,
    "clamp": 79,
    "glue": 80,
    "stapler": 81,
    "calculator": 82,
}

CATEGORY_ID_TO_NAME = {id_val: name for name, id_val in CATEGORY_IDS.items()}

NUM_KEYPOINTS = {
    "j_toy_airplane": 2,
    "plum": 2,
    "pear": 2,
    "lighters": 8,
    "kitchen_knife": 3,
    "box_6": 8,
    "cables": 2,
    "d_toy_airplane": 3,
    "box_2": 8,
    "h_toy_airplane": 6,
    "light_bulb": 2,
    "mug": 6,
    "sugar_box": 8,
    "glass_cup": 3,
    "box_8": 8,
    "glue": 9,
    "i_toy_airplane": 2,
    "airfilter": 2,
    "calculator": 8,
    "cup": 6,
    "racquetball": 1,
    "wire_cutter": 6,
    "wash_sponge": 9,
    "cables_in_transparent_bag": 4,
    "garbage_bags": 2,
    "apple": 2,
    "knife": 3,
    "stapler": 10,
    "chewing_gum_with_spray": 8,
    "clamp": 6,
    "wash_glove": 4,
    "lipstick": 2,
    "handsaw": 5,
    "k_toy_airplane": 12,
    "box_1": 8,
    "box_4": 8,
    "strawberry": 2,
    "flat_screwdriver": 3,
    "d_cups": 3,
    "box_3": 8,
    "shaving_cream": 8,
    "nosespray": 2,
    "cream_soap": 5,
    "mustard_bottle": 10,
    "box_5": 8,
    "scissor": 11,
    "potted_meat_can": 8,
    "deo": 2,
    "screw_valve": 6,
    "sponge": 8,
    "orange": 2,
    "desinfection": 2,
    "bottle_glass": 2,
    "toydog": 7,
    "chips_can": 2,
    "cracker_box": 8,
    "woodcube_a": 8,
    "coffeefilter": 8,
    "tomato_soup_can": 2,
    "box_7": 8,
    "peach": 2,
    "banana": 4,
    "power_drill": 6,
    "hairspray": 2,
    "phillips_screwdriver": 3,
    "lipcare": 8,
    "plastic_pipes": 2,
    "bottle_press_head": 3,
    "tennis_ball": 4,
    "cat_milk": 9,
    "lemon": 2,
    "a_toy_airplane": 10,
    "bookholder": 9,
    "f_toy_airplane": 2,
    "toothpaste": 8,
    "tape": 7,
    "handcream": 3,
    "c_toy_airplane": 6,
    "pneumatic_cylinder": 9,
    "b_cups": 3,
    "wineglass": 4,
    "bowl": 3,
}

classes_info = [
    dict(
        dataset_name=name,
        paper_info=dict(
            author='',
            title='',
            container='',
            year='',
            homepage='',
        ),
        keypoint_info={i: dict(name=str(i),
                id=i)
                for i in range(num_keypoints)},
        skeleton_info={
        },
        joint_weights=[1
        for i in range(num_keypoints)],
        sigmas=[1/num_keypoints
        for i in range(num_keypoints)]) 
    for name, num_keypoints in NUM_KEYPOINTS.items()]

dataset_channel = {}

for name, num_keypoint in NUM_KEYPOINTS.items():
    dataset_channel[name] = list(range(num_keypoint))