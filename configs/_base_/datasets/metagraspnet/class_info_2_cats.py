CATEGORY_IDS = {
    "cracker_box": 0,
    "power_drill": 8,
}

CATEGORY_ID_TO_NAME = {id_val: name for name, id_val in CATEGORY_IDS.items()}

NUM_KEYPOINTS = {
    "cracker_box": 8,
    "power_drill": 6,
}

classes_info = [
    dict(
        dataset_name=name,
        paper_info=dict(
            author='',
            title='',
            container='',
            year='',
            homepage='',
        ),
        keypoint_info={i: dict(name=str(i),
                id=i)
                for i in range(num_keypoints)},
        skeleton_info={
        },
        joint_weights=[1
        for i in range(num_keypoints)],
        sigmas=[1/num_keypoints
        for i in range(num_keypoints)]) 
    for name, num_keypoints in NUM_KEYPOINTS.items()]

dataset_channel = {}

for name, num_keypoint in NUM_KEYPOINTS.items():
    dataset_channel[name] = list(range(num_keypoint))