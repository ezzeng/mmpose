import glob
import json
import os

def get_class_info(categories_path):
    with open(os.path.join(categories_path, "category_ids.json"), "r") as f:
        category_ids = json.load(f)

    category_id_to_name = {id_val: name for name, id_val in category_ids.items()}

    keypoint_files = glob.glob(os.path.join(categories_path, "keypoint_points", "*"))
    num_keypoints = {}
    for filename in keypoint_files:
        with open(filename) as f:
            num_keypoints[os.path.basename(keypoint_files[0])[4:-4]] = len(f.read().splitlines())

    return category_ids, category_id_to_name, num_keypoints

