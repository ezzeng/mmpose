import numpy as np

from torch.utils.data import Sampler as _Sampler
import torch

from typing import Iterator, Iterable, Optional, Sequence, List, TypeVar, Generic, Sized, Union

from mmpose.core import sync_random_seed


class SingleCatBatchSampler(_Sampler):
    def __init__(self, data_source: Optional[Sized], batch_size: int, drop_last: bool, shuffle: bool, extend_batch=True, seed=0) -> None:
        super().__init__(data_source)
        self.data_source = data_source

        if not isinstance(batch_size, int) or isinstance(batch_size, bool) or \
                batch_size <= 0:
            raise ValueError("batch_size should be a positive integer value, "
                             "but got batch_size={}".format(batch_size))
        if not isinstance(drop_last, bool):
            raise ValueError("drop_last should be a boolean value, but got "
                             "drop_last={}".format(drop_last))
        self.batch_size = batch_size
        self.drop_last = drop_last
        self.shuffle = shuffle
        self.extend_batch = extend_batch

        self.num_classes = self.data_source.num_classes
        self.coco_category_indices = self.data_source.coco_category_indices

        self.epoch = 0
        self.seed = sync_random_seed(seed) if seed is not None else 0

    def category_iterator(self, category_id):
        indices = self.coco_category_indices[category_id]

        if self.shuffle:
            g = torch.Generator()
            g.manual_seed(self.epoch + self.seed)
            indices = indices[torch.randperm(len(indices), generator=g).tolist()]

        # extend batch to be more easily divisible
        if self.extend_batch:
            np.append(indices, indices[:self.batch_size - (len(indices) % self.batch_size)])

        batch = []
        for idx in indices:
            batch.append(idx)
            if len(batch) == self.batch_size:
                yield batch
                batch = []
        if len(batch) > 0 and not self.drop_last:
            yield batch

    def __iter__(self):
        category_iterators = [
            self.category_iterator(i) 
                for i, category_indices in enumerate(self.coco_category_indices) 
                if len(category_indices) > 0
            ]
        available_iterators = [i for i in range(len(category_iterators))]

        while len(available_iterators) > 0:
            available_iterator_id = np.random.choice(len(available_iterators))
            try:
                batch = next(category_iterators[available_iterators[available_iterator_id]])
                yield batch
            except StopIteration:
                available_iterators.pop(available_iterator_id)

        self.epoch += 1

    def __len__(self) -> int:
        length = 0
        for cat_indices in self.coco_category_indices:
            if self.drop_last:
                length += len(cat_indices) // self.batch_size  # type: ignore[arg-type]
            else:
                length += (len(cat_indices) + self.batch_size - 1) // self.batch_size  # type: ignore[arg-type]
        
        return length

    def set_epoch(self, epoch: int) -> None:
        r"""
        Sets the epoch for this sampler. When :attr:`shuffle=True`, this ensures all replicas
        use a different random ordering for each epoch. Otherwise, the next iteration of this
        sampler will yield the same ordering.

        Args:
            epoch (int): Epoch number.
        """
        self.epoch = epoch
