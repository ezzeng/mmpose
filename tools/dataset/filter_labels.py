import json
from unicodedata import category

category_ids_to_keep = [0, 8]
source_files = [
    "data/annotations/0517/train.json",
    "data/annotations/0517/val.json",
    "data/annotations/0517/test.json"
]

out_files = [
    "data/annotations/0517/2_categories/train.json",
    "data/annotations/0517/2_categories/val.json",
    "data/annotations/0517/2_categories/test.json"
]

for source, out_file in zip(source_files, out_files):
    with open(source, "r") as f:
        labels = json.load(f)

    annotations = [label for label in labels['annotations'] if label['category_id'] in category_ids_to_keep]
    labels['annotations'] = annotations

    with open(out_file, "w") as f:
        json.dump(labels, f, indent=4)

    print(f"filtered {source} to {out_file}")