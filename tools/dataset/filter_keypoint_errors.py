import json

with open("data/annotations/0517/2_categories/test.json", "r") as f:
    labels = json.load(f)

keypoint_errors = [label['image_id'] for label in labels['annotations'] if len(label['keypoints'][0]) == 0]

images = [image for image in labels['images'] if image['id'] not in keypoint_errors]

labels['images'] = images

with open("data/annotations/0517/2_categories/test.json", "w") as f:
    labels = json.dump(labels, f, indent=4)