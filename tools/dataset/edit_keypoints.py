import json
from unicodedata import category

source_files = [
    "data/annotations/0517/2_categories/train.json",
    "data/annotations/0517/2_categories/val.json",
    # "data/annotations/0517/2_categories/test.json"
]

out_files = [
    "data/annotations/0517/2_categories/train_edited.json",
    "data/annotations/0517/2_categories/val_edited.json",
    # "data/annotations/0517/2_categories/test_edited.json"
]

for source, out_file in zip(source_files, out_files):
    with open(source, "r") as f:
        labels = json.load(f)

    for label in labels['annotations']:
        label['keypoints'] = label['keypoints'][0]
        label['num_keypoints'] = int(len(label['keypoints']) / 3)

    with open(out_file, "w") as f:
        json.dump(labels, f, indent=4)

    print(f"filtered {source} to {out_file}")