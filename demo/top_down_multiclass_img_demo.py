# Copyright (c) OpenMMLab. All rights reserved.
import sys
# sys.path.append("/workspace/")
sys.path.append("/home/ezzeng/mmpose")
import os
import warnings
from argparse import ArgumentParser

from xtcocotools.coco import COCO

from mmpose.apis import (inference_top_down_multiclass_model, init_pose_model,
                         vis_pose_result)
from mmpose.datasets import DatasetInfo

import tqdm

from matplotlib import pyplot as plt
import numpy as np
import torch
import pickle

def _xywh2xyxy(bbox_xywh):
    """Transform the bbox format from xywh to x1y1x2y2.

    Args:
        bbox_xywh (ndarray): Bounding boxes (with scores),
            shaped (n, 4) or (n, 5). (left, top, width, height, [score])
    Returns:
        np.ndarray: Bounding boxes (with scores), shaped (n, 4) or
          (n, 5). (left, top, right, bottom, [score])
    """
    bbox_xyxy = bbox_xywh.copy()
    bbox_xyxy[2] = bbox_xyxy[2] + bbox_xyxy[0] - 1
    bbox_xyxy[3] = bbox_xyxy[3] + bbox_xyxy[1] - 1

    return bbox_xyxy


def main():
    """Visualize the demo images.

    Require the json_file containing boxes.
    """
    parser = ArgumentParser()
    parser.add_argument('pose_config', help='Config file for detection')
    parser.add_argument('pose_checkpoint', help='Checkpoint file')
    parser.add_argument('--img-root', type=str, default='', help='Image root')
    parser.add_argument(
        '--json-file',
        type=str,
        default='',
        help='Json file containing image info.')
    parser.add_argument(
        '--show',
        action='store_true',
        default=False,
        help='whether to show img')
    parser.add_argument(
        '--out-img-root',
        type=str,
        default='',
        help='Root of the output img file. '
        'Default not saving the visualization images.')
    parser.add_argument(
        '--device', default='cuda:0', help='Device used for inference')
    parser.add_argument(
        '--kpt-thr', type=float, default=0.3, help='Keypoint score threshold')
    parser.add_argument(
        '--radius',
        type=int,
        default=4,
        help='Keypoint radius for visualization')
    parser.add_argument(
        '--thickness',
        type=int,
        default=1,
        help='Link thickness for visualization')

    args = parser.parse_args()

    assert args.show or (args.out_img_root != '')

    coco = COCO(args.json_file)
    # build the pose model from a config file and a checkpoint file
    pose_model = init_pose_model(
        args.pose_config, args.pose_checkpoint, device=args.device.lower())

    dataset = pose_model.cfg.data['test']['type']
    dataset_info = pose_model.cfg.data['test'].get('dataset_info', None)
    if dataset_info is None:
        warnings.warn(
            'Please set `dataset_info` in the config.'
            'Check https://github.com/open-mmlab/mmpose/pull/663 for details.',
            DeprecationWarning)
    else:
        dataset_info = DatasetInfo(dataset_info)

    img_keys = list(coco.imgs.keys())

    # optional
    return_heatmap = True

    # e.g. use ('backbone', ) to return backbone feature
    output_layer_names = None

    if not os.path.exists(os.path.join(args.out_img_root, 'heatmap')):
        os.makedirs(os.path.join(args.out_img_root, 'heatmap'))

    # process each image
    for i in tqdm.tqdm(range(len(img_keys))):
        # get bounding box annotations
        image_id = img_keys[i]
        image = coco.loadImgs(image_id)[0]
        image_name = os.path.join(args.img_root, image['file_name'])
        ann_ids = coco.getAnnIds(image_id)

        # make object bounding boxes
        object_results = []
        for ann_id in ann_ids:
            object_ann = {}
            ann = coco.anns[ann_id]
            # bbox format is 'xywh'
            object_ann['bbox'] = _xywh2xyxy(ann['bbox'])
            object_ann['category_id'] = ann['category_id']
            if 'keypoints' in ann:
                object_ann['keypoints'] = np.reshape(ann['keypoints'], (-1, 3))
            else:
                object_ann['keypoints'] = np.array([0, 0, 0])
            object_results.append(object_ann)

        # test a single image, with a list of bboxes
        pose_results, returned_outputs, class_ids = inference_top_down_multiclass_model(
            pose_model,
            image_name,
            object_results,
            bbox_thr=None,
            format='xyxy',
            dataset=dataset,
            dataset_info=dataset_info,
            return_heatmap=return_heatmap,
            outputs=output_layer_names)

        if len(pose_results) > 0:

            if args.out_img_root == '':
                out_file = None
                out_file_heatmap = None
            else:
                os.makedirs(args.out_img_root, exist_ok=True)
                out_file = os.path.join(args.out_img_root, 'results', f'vis_{i}.jpg')
                out_file_heatmap = os.path.join(args.out_img_root, 'heatmap', f'vis_heatmap_{i}')
                out_file_groundtruth = os.path.join(args.out_img_root, 'groundtruth', f'vis_{i}.jpg')

            vis_pose_result(
                pose_model,
                image_name,
                pose_results,
                dataset=dataset,
                dataset_info=dataset_info,
                kpt_score_thr=args.kpt_thr,
                radius=args.radius,
                thickness=args.thickness,
                show=args.show,
                out_file=out_file)

            # vis_pose_result(
            #     pose_model,
            #     image_name,
            #     object_results,
            #     dataset=dataset,
            #     dataset_info=dataset_info,
            #     kpt_score_thr=args.kpt_thr,
            #     radius=args.radius,
            #     thickness=args.thickness,
            #     show=args.show,
            #     out_file=out_file_groundtruth)

            if return_heatmap:
                heatmap_info = []
                for class_id, returned_output in zip(class_ids, returned_outputs):
                    heatmap_info.append({
                        "class_id": class_id,
                        "heatmap": returned_output['heatmap']
                    })

                with open(out_file_heatmap+".pkl", "wb") as f:
                    pickle.dump(heatmap_info, f)


if __name__ == '__main__':
    main()
