# docker build -f ./docker/Dockerfile --rm -t mmpose .
IMAGE_DIR="/pub1/data/data_ifl/"
ANNOTATION_DIR="/home/ezzeng/image-to-coco-json-converter/output/keypoints-2"
dir=$(dirname $(realpath -s $0))
COCO_DIR="/pub1/ezzeng/coco/"

docker run --gpus all\
 --shm-size=8g \
 -it \
 -v $dir:/workspace \
 -v $IMAGE_DIR:$IMAGE_DIR \
 -v $ANNOTATION_DIR:$ANNOTATION_DIR \
 -v $COCO_DIR:$COCO_DIR \
 mmpose